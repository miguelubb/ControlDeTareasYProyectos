import java.util.Scanner;

/**
 * Created by miguel on 02-11-16.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("cual es tu nombre?");
        Scanner sc=new Scanner(System.in);
        sc.useDelimiter("\\n");
        String nombre=sc.next();
        System.out.println("Hola "+ nombre);
    }
}
